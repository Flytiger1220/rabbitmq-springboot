package com.example.rabbitmq.topic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class TopicController {

    @Autowired
    private SenderTopic sender;

    @GetMapping("/sendTopic")
    public String send(HttpServletRequest request, String routingKey, String msg) {
        sender.send(routingKey, msg);
        return "Send OK.";
    }
}
